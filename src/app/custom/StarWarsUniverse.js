import Entity from "./Entity";
export default class StarWarsUniverse {
    constructor() {
        this.entities = [];
    }
    async getapi(url) {
        const response = await fetch(url);
        const data = await response.json();
        return data;
    }
    async init() {
        let api_url = "https://swapi.boom.dev/api/"
        let api_res = await this.getapi(api_url)
        for (const [key, value] of Object.entries(api_res)) {
            let entity_data = await this.getapi(value)
            this.entities.push(new Entity(key, entity_data))
        }
    }
}
